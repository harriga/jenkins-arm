FROM jsurf/rpi-raspbian:latest

ARG PARAM_JENKINS_VERSION

# Other env variables
ENV JENKINS_HOME /var/jenkins_home
ENV JENKINS_SLAVE_AGENT_PORT 50000
ENV JENKINS_VERSION=${PARAM_JENKINS_VERSION}

# Install dependencies
RUN apt-get update \
  && apt-get install -y --no-install-recommends curl openjdk-8-jdk git\
  && rm -rf /var/lib/apt/lists/*

# Get Jenkins
RUN curl -fL -o /opt/jenkins.war http://mirror.serverion.com/jenkins/war/{$JENKINS_VERSION}/jenkins.war

# Expose volume
VOLUME ${JENKINS_HOME}

# Working dir
WORKDIR ${JENKINS_HOME}

# Expose ports
EXPOSE 8080 ${JENKINS_SLAVE_AGENT_PORT}

# Start Jenkins
CMD ["sh", "-c", "java -jar /opt/jenkins.war"]
